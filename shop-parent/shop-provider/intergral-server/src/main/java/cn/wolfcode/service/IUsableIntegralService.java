package cn.wolfcode.service;

import cn.wolfcode.domain.OperateIntergralVo;
import cn.wolfcode.domain.RefundVo;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;


@LocalTCC
public interface IUsableIntegralService {

    /**
     * TCC 一阶段 Try 方法, 实现用户积分资源预留
     *
     * @param vo  积分 vo 对象
     * @param ctx 上下文对象
     */
    @TwoPhaseBusinessAction(
            name = "tryPayment",
            commitMethod = "commitPayment",
            rollbackMethod = "rollbackPayment"
    )
    String tryPayment(@BusinessActionContextParameter(paramName = "vo") OperateIntergralVo vo,
                    BusinessActionContext ctx);

    /**
     * 二阶段提交方法, 真正扣除积分, 保存流水日志
     *
     * @param ctx 上下文对象
     * @return 返回交易订单号
     */
    void commitPayment(BusinessActionContext ctx);

    /**
     * 二阶段回滚方法, 如果一阶段失败, 对一阶段操作的数据进行回滚
     *
     * @param ctx
     */
    void rollbackPayment(BusinessActionContext ctx);


    String doPay(OperateIntergralVo vo);

    boolean doRefund(RefundVo refundVo);
}
