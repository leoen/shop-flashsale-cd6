package cn.wolfcode.mq.listener;

import cn.wolfcode.web.controller.OrderInfoController;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import static cn.wolfcode.mq.MQConstant.CANCEL_SECKILL_OVER_SIGE_CONSUMER_GROUP;
import static cn.wolfcode.mq.MQConstant.CANCEL_SECKILL_OVER_SIGE_TOPIC;

@RocketMQMessageListener(
        consumerGroup = CANCEL_SECKILL_OVER_SIGE_CONSUMER_GROUP,
        topic = CANCEL_SECKILL_OVER_SIGE_TOPIC,
        /* 将消息模式修改为广播模式 */
        messageModel = MessageModel.BROADCASTING
)
@Component
@Slf4j
public class CancelStockOverFlagMessageListener implements RocketMQListener<String> {

    @Override
    public void onMessage(String msg) {
        log.info("[取消本地标识] 收到取消本地标识消息, 准备删除本地标识: {}", msg);
        Long seckillId = Long.parseLong(msg);
        OrderInfoController.deleteKey(seckillId);
    }
}
