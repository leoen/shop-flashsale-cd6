package cn.wolfcode.service;


import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.domain.PayResult;
import cn.wolfcode.domain.RefundLog;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.mq.OrderMessage;

/**
 * Created by wolfcode
 */
public interface IOrderInfoService {

    OrderInfo selectByUserIdAndSeckillId(Long phone, Long seckillId);

    OrderInfo doSeckill(Long phone, SeckillProductVo sp);

    OrderInfo doSeckill(Long phone, Long seckillId, Integer time);

    OrderInfo selectByOrderNo(String orderNo);

    void failedRollback(OrderMessage message);

    void checkPayTimeout(OrderMessage message);

    String onlinePay(String orderNo);

    void alipaySuccess(PayResult result);

    void refund(String orderNo);

    void integralPay(String orderNo, Long phone);

    void integralRefundRollback(String orderNo);

    RefundLog selectRefundLogByOrderNo(String orderNo);
}
