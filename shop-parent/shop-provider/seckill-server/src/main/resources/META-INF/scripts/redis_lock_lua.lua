-- string 的key
local stringKey = KEYS[1]
-- string 的value
local stringVal = ARGV[1]
-- 过期时间
local expireAt = tonumber(ARGV[2])
-- check 值是否已存在, 不存在先插入key，并初始化值
local keyExist = redis.call("SETNX", KEYS[1], stringVal)
if (keyExist >= 1) then
    -- 设置过期时间
    -- EXPIREAT 设置时间戳(秒)
    -- EXPIRE 设置多少秒后过期
    redis.call("EXPIRE", KEYS[1], expireAt)
    return true
end

return false