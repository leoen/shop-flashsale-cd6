package cn.wolfcode.timer;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TimerTaskTest {

    @Test
    public void test() throws InterruptedException {
        ScheduledFuture<?> future = null;
        ScheduledFuture<?> finalFuture = future;
        TimerTask timerTask = new TimerTask() {

            int i = 0;

            @Override
            public void run() {
                System.out.println("当前时间: " + LocalDateTime.now());
                System.out.println(Thread.currentThread().getName()+"0000000000, " + i);
                if (i++ == 2) {
                    if (finalFuture != null) {
                        finalFuture.cancel(true);
                    }
                }


            }
        };

        // Timer timer = new Timer();
        // timer.schedule(timerTask, 0, 2000);

        ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
        future = service.scheduleAtFixedRate(timerTask, 0, 2, TimeUnit.SECONDS);


        TimeUnit.SECONDS.sleep(99999);
    }
}
