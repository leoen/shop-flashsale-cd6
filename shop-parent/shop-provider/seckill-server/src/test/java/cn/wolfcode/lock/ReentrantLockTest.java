package cn.wolfcode.lock;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockTest {

    @Test
    public void testCondition() throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Condition conditionB = lock.newCondition();

        CompletableFuture.runAsync(() -> {
            try {
                lock.lock();
                for (int i = 0; i < 10; i++) {
                    System.out.println("A");
                    Thread.sleep(1000);
                    // 唤醒其他线程
                    condition.signal();
                    // 当前线程等待
                    condition.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        CompletableFuture.runAsync(() -> {
            try {
                lock.lock();
                for (int i = 0; i < 10; i++) {
                    System.out.println("B");
                    Thread.sleep(1000);
                    // 唤醒其他线程
                    condition.signal();
                    // 当前线程等待
                    condition.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        TimeUnit.SECONDS.sleep(100000);
    }

    @Test
    public void testFair() throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(2);
        // 公平锁
//        ReentrantLock lock = new ReentrantLock(true);
        // 非公平锁
        ReentrantLock lock = new ReentrantLock(false);

        for (int i = 0; i < 10; i++) {
            service.execute(() -> {
                try {
                    lock.lock();
                    System.out.println(Thread.currentThread().getName());
                } finally {
                    lock.unlock();
                }
            });
        }

        TimeUnit.SECONDS.sleep(100000);
    }

    @Test
    public void test() throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(10);

        StockController stockController = new StockController();

        ReentrantLock lock = new ReentrantLock();

        for (int i = 0; i < 100; i++) {
            service.execute(() -> {
                try {
                    // 加锁
                    lock.lock();
                    int stock = stockController.getStock();
                    if (stock > 0) {
                        System.out.println(Thread.currentThread().getName() + ", 库存-1, 剩余库存=" + stockController.decrStock());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    // 释放锁
                    lock.unlock();
                }
            });
        }

        TimeUnit.SECONDS.sleep(100000);
    }

    class StockController {
        int stock = 10;

        public int decrStock() {
            return --stock;
        }

        public int getStock() {
            return stock;
        }
    }
}
