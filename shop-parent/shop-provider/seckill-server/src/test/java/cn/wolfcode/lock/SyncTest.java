package cn.wolfcode.lock;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class SyncTest {

    @Test
    public void test() throws InterruptedException {
        SyncController controller = new SyncController();
        SyncController controller1 = new SyncController();

        Integer i = 127;
        Integer i2 = Integer.valueOf(127);

        CompletableFuture.runAsync(() -> {
            controller.test3(i);
        });

        CompletableFuture.runAsync(() -> {
            controller.test5(i2);
        });


        TimeUnit.SECONDS.sleep(100000);
    }


    static class SyncController {

        public static synchronized void test1() {
            System.out.println("SyncController.test1");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public synchronized void test2() {
            System.out.println("SyncController.test2");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void test3(Integer i) {
            synchronized (i) {
                System.out.println("SyncController.test3");

                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void test4() {
            synchronized (SyncController.class) {
                System.out.println("SyncController.test4");
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void test5(Object i) {
            synchronized (i) {
                System.out.println("SyncController.test4");
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
