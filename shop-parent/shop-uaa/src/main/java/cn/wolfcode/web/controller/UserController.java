package cn.wolfcode.web.controller;

import cn.wolfcode.common.domain.UserInfo;
import cn.wolfcode.common.web.Result;
import cn.wolfcode.service.IUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/users")
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{phone}")
    public Result<UserInfo> getByPhone(@PathVariable String phone) {
        return Result.success(userService.getByPhone(phone));
    }

    @GetMapping("/update/{phone}")
    public Result<UserInfo> updateById(@PathVariable Long phone, String nickname) {
        UserInfo userInfo = new UserInfo();
        userInfo.setPhone(phone);
        userInfo.setNickName(nickname);

        userService.updateById(userInfo);
        return Result.success();
    }
}
