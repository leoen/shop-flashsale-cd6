package cn.wolfcode.handler;

import cn.wolfcode.domain.OrderInfo;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.annotation.CanalTable;
import top.javatool.canal.client.handler.EntryHandler;

@Slf4j
@Component
@CanalTable("t_order_info")
public class OrderInfoHandler implements EntryHandler<OrderInfo> {

    private final StringRedisTemplate redisTemplate;

    public OrderInfoHandler(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void update(OrderInfo before, OrderInfo after) {
        log.info("[订单数据同步] 收到修改订单数据 Before: {}", JSON.toJSONString(before));
        redisTemplate.opsForValue().set("orders::detail:" + after.getOrderNo(), JSON.toJSONString(after));
        log.info("[订单数据同步] 收到修改订单数据 After: {}", JSON.toJSONString(after));
    }

    @Override
    public void delete(OrderInfo orderInfo) {
        log.info("[订单数据同步] 收到删除订单消息: {}", JSON.toJSONString(orderInfo));
        redisTemplate.delete("orders::detail:" + orderInfo.getOrderNo());
    }
}
